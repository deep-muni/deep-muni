import react from "@vitejs/plugin-react";

import path from "path";
import { defineConfig } from "vite";

export default defineConfig({
  plugins: [react()],
  resolve: {
    alias: {
      "@components": path.resolve(__dirname, "src/components"),
      "@assets": path.resolve(__dirname, "src/assets"),
      "@utils": path.resolve(__dirname, "src/utils"),
    },
  },
  base: '',
  build: {
    rollupOptions: {
      output: {
        entryFileNames: 'main.bundle.[hash].js',
        chunkFileNames: 'chunk.bundle.[hash].js',
        assetFileNames: '[hash][extname]',
      },
    },
    assetsDir: '',
  },
});
