import { Component } from "react";

import Box from "@mui/material/Box";
import Grid2 from "@mui/material/Grid2";
import Typography from "@mui/material/Typography";

import { PERSONAL_DETAILS, PROFILES } from "@utils/constants.js";
import CustomAnchorButton from "@components/generic/CustomAnchorButton.jsx";

class Home extends Component {
  constructor(props) {
    super(props);
    this.profiles = Object.values(PROFILES);
  }

  onClick = (url) => window.open(url, "_blank");

  render() {
    return (
      <Box height="100vh" className="home-background" display="flex">
        <Grid2
          container
          direction="column"
          justifyContent="center"
          alignItems="center"
          width="100%"
        >
          <Grid2 className="m-b-40">
            <Typography
              component="p"
              color="#ffffff"
              className="home-name fs-100 ff-lc fw-600"
              textAlign="center"
            >
              {PERSONAL_DETAILS.NAME}
            </Typography>
          </Grid2>
          <Grid2 className="m-b-40">
            <Typography
              component="p"
              color="#ffffff"
              className="home-title fs-25 ls-5 ff-lc fw-500"
            >
              {PERSONAL_DETAILS.TITLE}
            </Typography>
          </Grid2>
          <Grid2>
            <Grid2 container spacing={5}>
              {this.profiles.map((profile, index) => {
                return (
                  <Grid2 xs={6} sm={3} key={index} textAlign="center">
                    <CustomAnchorButton
                      icon={profile.ICON}
                      title={profile.LABEL}
                      iconAttr={{ className: "profile-icon" }}
                      onClick={() => this.onClick(profile.URL)}
                    />
                  </Grid2>
                );
              })}
            </Grid2>
          </Grid2>
        </Grid2>
      </Box>
    );
  }
}

export default Home;
