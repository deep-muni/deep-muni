import moment from 'moment';
import PropTypes from "prop-types";
import { observer } from "mobx-react";
import React, { Component } from "react";
import { action, makeObservable, observable } from "mobx";

import Box from "@mui/material/Box";
import Slide from "@mui/material/Slide";
import Grid2 from "@mui/material/Grid2";
import Typography from "@mui/material/Typography";

import Timeline from "@mui/lab/Timeline";
import TimelineDot from "@mui/lab/TimelineDot";
import TimelineItem from "@mui/lab/TimelineItem";
import TimelineContent from "@mui/lab/TimelineContent";
import TimelineSeparator from "@mui/lab/TimelineSeparator";
import TimelineConnector from "@mui/lab/TimelineConnector";
import TimelineOppositeContent from "@mui/lab/TimelineOppositeContent";

import Utils from "@utils/utils";
import { WORK_HISTORY } from "@utils/constants";

class Experience extends Component {
  cState = {
    show: {},
    prevRatio: {},
  };

  observer = null;
  handleScroll = action((entries) => {
    entries.forEach((entry) => {
      const key = entry.target.id;
      const currentRatio = entry.intersectionRatio;
      this.cState.show[key] = currentRatio > (this.cState.prevRatio[key] || 0);
      this.cState.prevRatio[key] = currentRatio;
    });
  });

  constructor(props) {
    super(props);
    this.workHistory = Object.values(WORK_HISTORY);
    this.totalExperience = Utils.getTotalExperience();
    makeObservable(this, {
      cState: observable,
      handleScroll: action,
    });
  }

  componentDidMount() {
    this.addIntersectionObserver();
  }

  componentWillUnmount() {
    if (this.observer) {
      this.observer.disconnect();
    }
  }

  addIntersectionObserver = () => {
    this.observer = new IntersectionObserver(this.handleScroll, {
      threshold: 1,
    });

    this.workHistory.forEach((company, index) => {
      const element = document.getElementById("content-" + index);
      if (element) {
        this.observer.observe(element);
      }
    });
  };

  getDuration = (doj, lwd) => {
    return `${moment(doj).format('MMMM YYYY')} - ${lwd ? `${moment(lwd).format('MMMM YYYY')}` : "Present"}`;
  };

  render() {
    const { mobile } = this.props;


    return (
      <Box px={4} py={6}>
        <Typography variant="h5" textAlign="center" className="m-b-40">
          Work Experience ({this.totalExperience})
        </Typography>
        <Timeline position={mobile ? "right" : "alternate"}>
          {this.workHistory.map((company, index) => {
            const doj = new Date(company.DURATION.DOJ); // Assuming these are Date strings
            const lwd = company.DURATION.LWD
              ? new Date(company.DURATION.LWD)
              : "";
            const id = "content-" + index;
            const isEven = index % 2 === 0;

            return (
              <TimelineItem key={index} className="m-b-40" id={id}>
                {!mobile && (
                  <TimelineOppositeContent
                    className={isEven ? "d-f ai-c jc-fe" : "d-f ai-c jc-fs"}
                  >
                    <Slide
                      in={this.cState.show[id]}
                      direction={isEven ? "right" : "left"}
                    >
                      <Typography
                        variant={this.props.mobile ? "body2" : "body1"}
                      >
                        {this.getDuration(doj, lwd)}
                      </Typography>
                    </Slide>
                  </TimelineOppositeContent>
                )}
                <TimelineSeparator>
                  <TimelineConnector />
                  <TimelineDot
                    style={{
                      backgroundColor: "transparent",
                      boxShadow: "none",
                    }}
                  >
                    {company.PNG_FORMAT ? (
                      <img
                        src={company.LOGO}
                        width="100px"
                        alt={company.NAME}
                      />
                    ) : (
                      React.createElement(company.LOGO)
                    )}
                  </TimelineDot>
                  <TimelineConnector />
                </TimelineSeparator>
                <Slide
                  in={this.cState.show[id]}
                  direction={isEven ? "left" : "right"}
                >
                  <TimelineContent>
                    <Box p={2} className="experience-details">
                      <Grid2 container flexDirection="column">
                        <Grid2 xs={12}>
                          <Typography variant={mobile ? "body1" : "h6"}>
                            {company.DESIGNATION}
                          </Typography>
                        </Grid2>
                        <Grid2 xs={12}>
                          <Typography
                            variant={mobile ? "body2" : "body1"}
                          >{`${company.NAME}, ${company.LOCATION}`}</Typography>
                        </Grid2>
                        {mobile && (
                          <Grid2 xs={12}>
                            {this.getDuration(doj, lwd)}
                          </Grid2>
                        )}
                      </Grid2>
                    </Box>
                  </TimelineContent>
                </Slide>
              </TimelineItem>
            );
          })}
        </Timeline>
      </Box>
    );
  }
}

Experience.propTypes = {
  mobile: PropTypes.bool.isRequired,
};

export default observer(Experience);
