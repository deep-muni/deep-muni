import { Component } from "react";
import PropTypes from "prop-types";

import Zoom from "@mui/material/Zoom";
import Tooltip from "@mui/material/Tooltip";
import IconButton from "@mui/material/IconButton";

class CustomAnchorButton extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      title,
      icon: Icon,
      onClick,
      buttonAttr = {},
      iconAttr = {},
    } = this.props;

    return (
      <Tooltip title={title} arrow TransitionComponent={Zoom}>
        <IconButton aria-label={title} onClick={onClick} {...buttonAttr}>
          <Icon {...iconAttr} />
        </IconButton>
      </Tooltip>
    );
  }
}

CustomAnchorButton.propTypes = {
  title: PropTypes.string.isRequired,
  icon: PropTypes.elementType,
  onClick: PropTypes.func.isRequired,
  buttonAttr: PropTypes.object,
  iconAttr: PropTypes.object,
};

export default CustomAnchorButton;
