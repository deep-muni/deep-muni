import { Fragment, useEffect } from "react";

import { useTheme } from "@mui/material/styles";
import useMediaQuery from "@mui/material/useMediaQuery";

import Home from "@components/home/Home.jsx";
import Experience from "@components/experience/Experience.jsx";

const App = () => {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.down("sm"));

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);

  return (
    <Fragment>
      <Home />
      <Experience mobile={matches} />
    </Fragment>
  );
};

export default App;
