import EmailIcon from "@mui/icons-material/Email";
import GitHubIcon from "@mui/icons-material/GitHub";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import DescriptionIcon from "@mui/icons-material/Description";

import Freestone from "@assets/png/fs.png";
import Resume from "@assets/pdf/Resume.pdf";
import MediaNet from "@assets/svg/MediaNet.jsx";
import Incubyte from "@assets/svg/Incubyte.jsx";
import Dalhousie from "@assets/svg/Dalhousie.jsx";
import Accenture from "@assets/svg/Accenture.jsx";
import CanadaRevenueAgency from "@assets/svg/CanadaRevenueAgency.jsx";

const PERSONAL_DETAILS = {
  NAME: "Deep Muni",
  EMAIL: "deep.muni94@gmail.com",
  TITLE: "Senior Full Stack Developer",
  GITHUB_PROFILE: "https://github.com/deep-muni/",
  LINKEDIN_PROFILE: "https://www.linkedin.com/in/deep-muni/",
};

const PROFILES = [
  {
    LABEL: "GitHub",
    ICON: GitHubIcon,
    URL: PERSONAL_DETAILS.GITHUB_PROFILE,
  },
  {
    LABEL: "LinkedIn",
    ICON: LinkedInIcon,
    URL: PERSONAL_DETAILS.LINKEDIN_PROFILE,
  },
  {
    LABEL: "Resume",
    ICON: DescriptionIcon,
    URL: Resume,
  },
  {
    LABEL: "Email",
    ICON: EmailIcon,
    URL: `mailto:${PERSONAL_DETAILS.EMAIL}`,
  },
];

const WORK_HISTORY = [
  {
    NAME: "Incubyte",
    DESIGNATION: "Software Craftsperson",
    LOCATION: "Mumbai, India",
    LOGO: Incubyte,
    PNG_FORMAT: false,
    URL: "https://www.incubyte.co/",
    DURATION: { CURRENT: true, CONSIDER: true, DOJ: new Date("08/20/2024") },
  },
  {
    NAME: "Freestone Infotech (Privacera)",
    DESIGNATION: "Senior Software Engineer",
    LOCATION: "Mumbai, India",
    LOGO: Freestone,
    PNG_FORMAT: true,
    URL: "https://freestoneinfotech.com",
    DURATION: {
      CURRENT: false,
      CONSIDER: true,
      DOJ: new Date("01/06/2022"),
      LWD: new Date("08/06/2024"),
    },
  },
  {
    NAME: "Canada Revenue Agency",
    DESIGNATION: "Software Developer",
    LOCATION: "Ottawa, Canada",
    LOGO: CanadaRevenueAgency,
    PNG_FORMAT: false,
    URL: "https://www.canada.ca/en/revenue-agency.html",
    DURATION: {
      CURRENT: false,
      CONSIDER: true,
      DOJ: new Date("01/06/2021"),
      LWD: new Date("01/05/2022"),
    },
  },
  {
    NAME: "Dalhousie University",
    DESIGNATION: "Graduate Teaching Assistant",
    LOCATION: "Halifax, Canada",
    LOGO: Dalhousie,
    PNG_FORMAT: false,
    URL: "https://www.dal.ca/",
    DURATION: {
      CURRENT: false,
      CONSIDER: false,
      DOJ: new Date("09/01/2020"),
      LWD: new Date("12/31/2020"),
    },
  },
  {
    NAME: "Media.Net",
    DESIGNATION: "Associate UI Developer",
    LOCATION: "Mumbai, India",
    LOGO: MediaNet,
    PNG_FORMAT: false,
    URL: "https://www.media.net/",
    DURATION: {
      CURRENT: false,
      CONSIDER: true,
      DOJ: new Date("03/11/2019"),
      LWD: new Date("06/28/2019"),
    },
  },
  {
    NAME: "Accenture Solutions",
    DESIGNATION: "Application Development Analyst",
    LOCATION: "Mumbai, India",
    LOGO: Accenture,
    PNG_FORMAT: false,
    URL: "https://www.accenture.com/in-en/accenture-solutions-private",
    DURATION: {
      CURRENT: false,
      CONSIDER: true,
      DOJ: new Date("08/30/2016"),
      LWD: new Date("03/06/2019"),
    },
  },
];

export { PROFILES, WORK_HISTORY, PERSONAL_DETAILS };
