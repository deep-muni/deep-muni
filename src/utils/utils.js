import moment from 'moment';

import { WORK_HISTORY} from "@utils/constants";

const Utils = {};

Utils.getTotalExperience = () => {
    const totalMilliseconds = WORK_HISTORY.reduce((acc, { DURATION }) => {
        if (!DURATION.CONSIDER) return acc;
        const { DOJ: start, LWD: end = new Date() } = DURATION;
        return acc + (end - start);
    }, 0);

    const duration = moment.duration(totalMilliseconds);
    const years = duration.years();
    const months = duration.months();
    const days = duration.days();

    return `${years} years, ${months} months, ${days} days`;
};

export default Utils;